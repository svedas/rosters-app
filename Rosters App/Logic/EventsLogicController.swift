//
//  EventsLogicController.swift
//  Rosters App
//
//  Created by Mantas Svedas on 1/21/20.
//  Copyright © 2020 Mantas Svedas. All rights reserved.
//

import Foundation

class EventsLogicController {
    typealias EventHandler = (EventsState) -> Void
    var events: [Event] = []
    
    var apiService: APIServiceProtocol = APIService()
    var coreDataService: DatabaseServiceProtocol = RealmService()
    var userDefaultsService: UserDefaultsSeriviceProtocol = UserDefaultsSerivice()
    
    func loadEventData(fromTeam team: Team, handler: @escaping EventHandler) {
        if !userDefaultsService.isUpdateTimeExists(forEntity: UpdateTime.Event) {
            debugPrint("Very bad. Events should have been created")
            handler(.failed(UserDefaultsError.notInitialized))
        }
        
        if userDefaultsService.shouldUpdate(forEntity: UpdateTime.Event)  {
            updateTeamEventsFromApiIntoCoreData(fromTeam: team, withHandler: handler)
        } else {
            getTeamEventsFromCoreData(fromTeam: team, withHandler: handler)
        }
    }
    
    // MARK: Helper functions
    
    private func updateTeamEventsFromApiIntoCoreData(fromTeam team: Team, withHandler handler: @escaping EventHandler) {
        getTeamEventsFromApi(fromTeam: team, withHandler: { handlerState in
            switch handlerState {
            case .presenting(let events):
                self.updateEvents(fromTeam: team, with: events)
                handler(.presenting(events))
            case .failed(let error):
                handler(.failed(error))
            }
        })
        debugPrint("Events for \(team.name ) Loading from API")
    }
    
    private func getTeamEventsFromApi(fromTeam team: Team, withHandler handler: @escaping EventHandler) {
        apiService.getTeamEventsAF(fromTeam: team ) { [weak self] (result) in
            guard let self = self else { return }
            switch result {
            case .success(let events):
                self.events = events.results ?? []
                handler(.presenting(self.events))
            case .failure(let error):
                print("Error getting teams")
                handler(.failed(error))
            }
        }
    }
    
    private func updateEvents(fromTeam team: Team, with events: [Event]) {
        coreDataService.deleteAllEventDataWithTeamId(team: team )
        DispatchQueue.main.async {
            self.coreDataService.createEvents(events: events)
        }
    }
    
    private func getTeamEventsFromCoreData(fromTeam team: Team, withHandler handler: @escaping EventHandler) {
        self.events = coreDataService.retrieveEvents(team: team )
        handler(.presenting(events))
        debugPrint("Events for \(team.name ) Loading from Database")
    }
}

