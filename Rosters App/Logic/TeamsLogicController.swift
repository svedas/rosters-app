//
//  TeamsViewDataLoader.swift
//  Rosters App
//
//  Created by Mantas Svedas on 1/21/20.
//  Copyright © 2020 Mantas Svedas. All rights reserved.
//

import Foundation

class TeamsLogicController {
    typealias TeamHandler = (TeamsState) -> Void
    var teams: [Team] = []
    
    var apiService: APIServiceProtocol = APIService()
    var coreDataService: DatabaseServiceProtocol = RealmService()
    var userDefaultsService: UserDefaultsSeriviceProtocol = UserDefaultsSerivice()
    
    func loadTeamData(then handler: @escaping TeamHandler) {
        if !userDefaultsService.isUpdateTimeExists(forEntity: UpdateTime.Team) {
            getEverythingFromApiAndLoadInCoreData(withHandler: handler)
            return
        }
        
        if userDefaultsService.shouldUpdate(forEntity: UpdateTime.Team) {
            updateTeamsFromApiToCoreData(withHandler: handler)                          // Teams from API
        } else {
            getTeamsFromCoreData(withHandler: handler)                                  // Teams from Core
        }

        if userDefaultsService.shouldUpdate(forEntity: UpdateTime.Player) {
            updateEventsAndPlayersFromApiToCoreData(fromTeams: teams)                   // Events and Players from API
            return
        }

        if userDefaultsService.shouldUpdate(forEntity: UpdateTime.Event) {
            updateOnlyEventsFromApiToCoreData(fromTeams: teams)                         // Events from API
        }
    }
    
    // MARK: - Load teams from API and save into CoreData
    
    private func updateTeamsFromApiToCoreData(withHandler handler: @escaping TeamHandler) {
        getTeamsFromApi { handlerState in
            switch handlerState {
            case .loading:
                self.teams = []
            case .presenting(let teams):
                self.updateTeams(with: teams)
                handler(.presenting(teams))
            case .failed(let error):
                debugPrint(error)
                print("Error getting teams")
            }
        }
        debugPrint("Teams Database reload")
    }
    
    private func getTeamsFromApi(withHandler handler: @escaping TeamHandler) {
        apiService.getAllTeamsAF { [weak self] (result) in
            guard let self = self else { return }
            switch result {
            case .success(let teams):
                self.teams = teams.teams ?? []
                handler(.presenting(self.teams))
            case .failure( _):
                handler(.presenting([]))
            }
        }
    }
    
    private func updateTeams(with teams: [Team]) {
        coreDataService.deleteAllData(entity: "TeamData")
        saveTeams(teams: teams)
    }
    
    private func saveTeams(teams: [Team]) {
        coreDataService.createTeams(teams: teams)
        userDefaultsService.setUpdateTime(withValue: Date(), forEntity: UpdateTime.Team)
    }
    
    // MARK: Load teams from core data
    
    private func getTeamsFromCoreData(withHandler handler: @escaping TeamHandler) {
        self.teams = coreDataService.retrieveTeams()
        handler(.presenting(self.teams))
        debugPrint("Teams load from Database")
    }
    
    // MARK: Load events and players from API and save into CoreData
    
    private func updateEventsAndPlayersFromApiToCoreData(fromTeams teams: [Team]) {
        coreDataService.deleteAllData(entity: "EventData")
        coreDataService.deleteAllData(entity: "PlayerData")
        for team in teams {
            updateEvents(fromTeam: team)
            updatePlayers(fromTeam: team)
        }
        debugPrint("Events and players Database reload")
    }
    
    private func updateOnlyEventsFromApiToCoreData(fromTeams teams: [Team]) {
        coreDataService.deleteAllData(entity: "EventData")
        for team in teams {
            updateEvents(fromTeam: team)
        }
        debugPrint("Events Database reload")
    }
    
    // ##############################################
    
    private func updateEvents(fromTeam team: Team) {
        apiService.getTeamEventsAF(fromTeam: team ) { (result) in
            switch result {
            case .success(let events):
                self.saveEvents(fromTeam: team, events: events)
            case .failure( _):
                print("Error getting teams")
            }
        }
    }
    
    private func saveEvents(fromTeam team: Team, events: EventsResponse) {
        DispatchQueue.main.async {
            self.coreDataService.createEvents(events: events.results)
            self.userDefaultsService.setUpdateTime(withValue: Date(), forEntity: UpdateTime.Event)
        }
    }
    
    // ##############################################
    
    private func updatePlayers(fromTeam team: Team) {
        apiService.getTeamPlayersAF(fromTeam: team) { (result) in
            switch result {
            case .success(let players):
                self.savePlayers(fromTeam: team, players: players)
            case .failure( _):
                print("Error getting teams")
            }
        }
    }
    
    private func savePlayers(fromTeam team: Team, players: PlayersResponse) {
        DispatchQueue.main.async {
            self.coreDataService.createPlayers(players: players.player)
            self.userDefaultsService.setUpdateTime(withValue: Date(), forEntity: UpdateTime.Player)
        }
    }
    
    // MARK: First launch setup
    
    private func getEverythingFromApiAndLoadInCoreData(withHandler handler: @escaping TeamHandler) {
        apiService.getAllTeamsAF { [weak self] (result) in
            guard let self = self else { return }
            switch result {
            case .success(let teams):
                self.teams = teams.teams ?? []
                self.updateEverything(with: self.teams)
                handler(.presenting(self.teams))
            case .failure( _):
                handler(.presenting([]))
                print("Error getting teams")
            }
        }
        debugPrint("First time lauch. Database load")
    }
    
    private func updateEverything(with teams: [Team]) {
        DispatchQueue.main.async {
            self.updateTeams(with: self.teams)
            for team in teams {
                self.updateEvents(fromTeam: team)
                self.updatePlayers(fromTeam: team)
            }
        }
    }
}

