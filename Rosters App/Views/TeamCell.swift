//
//  TeamCell.swift
//  Rosters App
//
//  Created by Mantas Svedas on 1/8/20.
//  Copyright © 2020 Mantas Svedas. All rights reserved.
//

import UIKit

class TeamCell: UITableViewCell {
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var cellContentView: UIView! //
    @IBOutlet weak var shadowView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
