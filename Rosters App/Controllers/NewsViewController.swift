//
//  NewsViewController.swift
//  Rosters App
//
//  Created by Mantas Svedas on 1/9/20.
//  Copyright © 2020 Mantas Svedas. All rights reserved.
//

import UIKit

class NewsViewController: UIViewController {
    private let logicController: EventsLogicController = EventsLogicController()
    @IBOutlet private weak var newsTableView: UITableView!

    var team: Team?
    var events: [Event] = [] 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.accessibilityIdentifier = "NewsVC"
        render(state: .presenting([]))
        logicController.loadEventData(fromTeam: team ?? Team()) { [weak self] state in
            guard let self = self else { return }
            self.render(state: state)
        }
    }
}

extension NewsViewController {
    private func render(state: EventsState){
        switch state {
            case .presenting(let events):
                self.events = events
                self.newsTableView.reloadData()
            
            case .failed(let error):
                self.events = []
                debugPrint(error)
        }
    }
}

extension NewsViewController:  UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return events.count > 0 ? events.count : 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let newsCell = tableView.dequeueReusableCell(withIdentifier: "newsCell",
                                                           for: indexPath) as? NewsCell else { return UITableViewCell()}
        
        if events.count == 0 {
            //isEmpty = true
            newsCell.dateLabel.text = "-"
            newsCell.firstTeamLabel.text = "???"
            newsCell.secondTeamLabel.text = "???"
        } else {
            newsCell.dateLabel.text = events[indexPath.row].date
            newsCell.firstTeamLabel.text = events[indexPath.row].firstTeamName
            newsCell.secondTeamLabel.text = events[indexPath.row].secondTeamName
        }
        return newsCell
    }
}
