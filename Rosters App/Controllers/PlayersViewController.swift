//
//  PlayersViewController.swift
//  Rosters App
//
//  Created by Mantas Svedas on 1/9/20.
//  Copyright © 2020 Mantas Svedas. All rights reserved.
//

import UIKit

class PlayersViewController: UIViewController {
    private let logicController: PlayersLogicController = PlayersLogicController()
    @IBOutlet private weak var playersTableView: UITableView!
    var team: Team?
    var players: [Player] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.accessibilityIdentifier = "PlayersVC"
        render(state: .presenting([]))
        logicController.loadPlayersData(fromTeam: team ?? Team()) { [weak self] state in
            guard let self = self else { return }
            self.render(state: state)
        }
    }
}

extension PlayersViewController {
    private func render(state: PlayersState) {
        switch state {
            case .presenting(let players):
                self.players = players
                self.playersTableView.reloadData()
            
            case .failed(let error):
                self.players = []
                debugPrint(error)
        }
    }
}

extension PlayersViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let nextViewController = segue.destination as? ProfileViewController
        
        guard let selectedPlayerCell = sender as? PlayerCell else { return }
        let indexPath = playersTableView.indexPath(for: selectedPlayerCell)
        let selectedPlayer = players[indexPath?.row ?? 0]
        
        nextViewController?.player = selectedPlayer
    }
}

extension PlayersViewController: UITableViewDataSource, UITableViewDelegate  { //TableView methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return players.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let playerCell = tableView.dequeueReusableCell(withIdentifier: "playerCell",
                                                             for: indexPath) as? PlayerCell else { return UITableViewCell() }
        playerCell.playerLabel.text = players[indexPath.row].name
        playerCell.playerImageView.imageFromURL(urlString: players[indexPath.row].icon
            ?? "https://sweettutos.com/wp-content/uploads/2015/12/placeholder.png" )
        
        return playerCell
    }
}
