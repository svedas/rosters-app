//
//  ViewController.swift
//  Rosters App
//
//  Created by Mantas Svedas on 1/8/20.
//  Copyright © 2020 Mantas Svedas. All rights reserved.
//

import UIKit
import CoreData

class TeamsViewController: UIViewController {
    private let logicController: TeamsLogicController = TeamsLogicController()
    @IBOutlet private weak var teamTableView: UITableView!
    private var vSpinner : UIView?
    private var teams: [Team] = [] {
        didSet {
            self.teamTableView.reloadData()
        }
    }
    var isEmpty = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.accessibilityIdentifier = "TeamsVC"
        render(state: .loading)
        logicController.apiService = APIService()
        logicController.loadTeamData { [weak self] state in
            guard let self = self else { return }
            self.render(state: state)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let nextViewController = segue.destination as? SegmentedViewController
        
        guard let selectedTeamCell = sender as? TeamCell else { return }
        let indexPath = teamTableView.indexPath(for: selectedTeamCell)
        let selectedTeam = !isEmpty ? teams[indexPath?.row ?? 0] : Team()
        
        nextViewController?.team = selectedTeam
    }
}

extension TeamsViewController {
    private func render(state: TeamsState) {
        switch state {
            case .loading:
                self.teams = []
                vSpinner = self.showSpinner(onView: self.view)
            
            case .presenting(let teams):
                self.removeSpinner(vSpinner: vSpinner ?? UIView())
                self.teams = teams
                self.isEmpty = false
            
            case .failed(let error):
                debugPrint(error)
                self.removeSpinner(vSpinner: vSpinner ?? UIView())
                self.teams = []
        }
    }
}

extension TeamsViewController: UITableViewDataSource, UITableViewDelegate { // TableView Functions
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return teams.count > 0 ? teams.count : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "teamCell2",
                                                       for: indexPath) as? TeamCell else { return UITableViewCell()}
        cell.shadowView.layer.cornerRadius = 4
        //cell.shadowView.layer.masksToBounds = true
        if teams.count == 0 {
            isEmpty = true
            cell.iconView.imageFromURL(urlString: "https://sweettutos.com/wp-content/uploads/2015/12/placeholder.png")
            cell.nameLabel.text = "No teams found"
            cell.descriptionLabel.text = "Please check your internet connection or reload the app"
            cell.shadowView.dropShadow()
        } else {
            cell.iconView.imageFromURL(urlString: teams[indexPath.row].icon)
            cell.nameLabel.text = teams[indexPath.row].name
            cell.descriptionLabel.text = teams[indexPath.row].teamDescription
            cell.shadowView.dropShadow()
        }
        return cell
    }
}
